module QRHouseholder

using LinearAlgebra

export householder_qr, householder_qr2

function householder_matrix(v)
    if norm(v) > eps()
        return I - (2/dot(v,v)) * (v * v')
    else
        return Matrix{typeof(v[1])}(I, size(v[:])[1], size(v[:])[1])
    end
end

function householder_matrix(β, v)
    return I - β * (v * v')
end

function householder_direction(source)
    σ = dot(source[2:end], source[2:end])
    v = [1; source[2:end]]
    if (σ < eps()/10 && source[1] >= 0)
        β = 0
    elseif (σ < eps()/10 && source[1] < 0)
        β = 2
    else
        μ = sqrt(source[1]^2 + σ)
        if (source[1] <= 0)
            v[1] = source[1] - μ
        else
            v[1] = -σ/(source[1]+μ)
        end
        β = 2*v[1]^2 / (σ + v[1]^2)
        v = v/v[1]
    end
    return (β, v)
end

function householder_direction(source, target)
    w = target/norm(target)
    λ = norm(source)
    return (source - λ * w)/norm(source - λ * w)
end

function householder_qr(matrix)
    (nr, nc) = size(matrix)
    orth_matrix = Matrix{Float64}(I, nr, nc)
    r_matrix = copy(matrix)
    for col_idx = 1:nc
        source_vector = r_matrix[col_idx:nr, col_idx]
        target_vector = zeros(nr - col_idx + 1)
        target_vector[1] = 1.0
        direction_low_dim = householder_direction(source_vector, target_vector)
        direction = zeros(nr)
        direction[col_idx:nr] = direction_low_dim
        householder_transform = householder_matrix(direction[:])
        r_matrix = householder_transform' * r_matrix
        orth_matrix = orth_matrix * householder_transform
    end
    return (orth_matrix, r_matrix)
end

function householder_qr2(matrix)
    (nr, nc) = size(matrix)
    orth_matrix = Matrix{Float64}(I, nr, nc)
    r_matrix = copy(matrix)
    for col_idx = 1:nc
        source_vector = r_matrix[col_idx:nr, col_idx]
        target_vector = zeros(nr - col_idx + 1)
        target_vector[1] = 1.0
        (β, direction_low_dim) = householder_direction(source_vector)
        direction = zeros(nr)
        direction[col_idx:nr] = direction_low_dim
        householder_transform = householder_matrix(β, direction[:])
        r_matrix = householder_transform' * r_matrix
        orth_matrix = orth_matrix * householder_transform
    end
    return (orth_matrix, r_matrix)
end

end # module
