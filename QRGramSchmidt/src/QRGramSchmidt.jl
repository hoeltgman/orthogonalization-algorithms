module QRGramSchmidt

using LinearAlgebra

export qr_classical_gram_schmidt, qr_modified_gram_schmidt, qr_reorth, qr_reorth_rank

"""
    Q, R = qr_classical_gram_schmidt(matrix)

Perform QR decomposition on the input matrix `matrix` using a classical
Gram-Schmidt orthogonalization.
"""
function qr_classical_gram_schmidt(matrix)
    # perform a QR decomposition using classical Gram Schmidt
    num_vectors = size(matrix)[2]
    orth_matrix = copy(matrix)
    r_matrix = zeros(num_vectors, num_vectors)
    for vec_idx = 1:num_vectors
        for span_base_idx = 1:(vec_idx - 1)
            # substrack sum
            r_matrix[span_base_idx, vec_idx] = dot(orth_matrix[:, span_base_idx], matrix[:, vec_idx])
            orth_matrix[:, vec_idx] -= r_matrix[span_base_idx, vec_idx] * orth_matrix[:, span_base_idx]
        end
        # normalise vector
        orth_matrix[:, vec_idx] = orth_matrix[:, vec_idx] / norm(orth_matrix[:, vec_idx])
        r_matrix[vec_idx, vec_idx] = dot(orth_matrix[:, vec_idx], matrix[:, vec_idx])
    end
    return (orth_matrix, r_matrix)
end # function

"""
    Q, R = qr_modified_gram_schmidt(matrix)

Perform QR decomposition on the input matrix `matrix` using a modified
Gram-Schmidt orthogonalization.
"""
function qr_modified_gram_schmidt(matrix)
    # perform a QR decomposition using modified Gram Schmidt
    num_vectors = size(matrix)[2]
    orth_matrix = copy(matrix)
    r_matrix = zeros(num_vectors, num_vectors)
    for vec_idx = 1:num_vectors
        orth_matrix[:, vec_idx] = orth_matrix[:, vec_idx] / norm(orth_matrix[:, vec_idx])
        r_matrix[vec_idx, vec_idx] = dot(orth_matrix[:, vec_idx], matrix[:, vec_idx])
        for span_base_idx = (vec_idx + 1):num_vectors
            # perform block step
            r_matrix[vec_idx, span_base_idx] += dot(orth_matrix[:, span_base_idx], orth_matrix[:, vec_idx])
            orth_matrix[:, span_base_idx] -= dot(orth_matrix[:, span_base_idx], orth_matrix[:, vec_idx]) * orth_matrix[:, vec_idx]
        end
    end
    return (orth_matrix, r_matrix)
end # function

"""
    Q, R = qr_reorth(matrix)

Perform QR decomposition on the input matrix `matrix` using a modified
Gram-Schmidt orthogonalization with reorthogonalization
"""
function qr_reorth(matrix, reorth_threshold=10, update_r=false)
    # perform a QR decomposition using classical Gram Schmidt with reorthogonalization
    # We use the rule of thumb suggested by Rutishauser to decide when to reorthogonalise
    # Note that this version does not work for rank deficient setups!
    #
    # reorth_threshold should be a number > 1. Values close to 1 cause more orthogonalizations,
    # larger values cause less orthogonalizations. The default value 10 cause roughly 1 orthogonalization
    # step
    num_vectors = size(matrix)[2]
    orth_matrix = copy(matrix)
    r_matrix = zeros(num_vectors, num_vectors)
    for vec_idx = 1:num_vectors
        norm_orth_column = 0   
        perform_reorthogonalization = true
        while (perform_reorthogonalization)
            norm_current_column = norm(orth_matrix[:, vec_idx])    
            for span_base_idx = 1:(vec_idx - 1) # orthogonalization
                projection_length = dot(orth_matrix[:, span_base_idx], orth_matrix[:, vec_idx])
                if (norm_orth_column == 0)
                    # do not update R when performing reorthogonalization
                    # norm_orth_column is exactly 0 on the first pass thgough
                    # this ensures that R gets updated properly
                    r_matrix[span_base_idx, vec_idx] = projection_length
                elseif (update_r)
                    r_matrix[span_base_idx, vec_idx] += projection_length
                end
                orth_matrix[:, vec_idx] -= projection_length * orth_matrix[:, span_base_idx]
            end
            norm_orth_column = norm(orth_matrix[:, vec_idx])
            perform_reorthogonalization = (norm_orth_column < norm_current_column / reorth_threshold)
        end
        # normalise vector
        # r_matrix[vec_idx, vec_idx] = norm(orth_matrix[:, vec_idx])
        orth_matrix[:, vec_idx] = orth_matrix[:, vec_idx] / norm(orth_matrix[:, vec_idx])
        r_matrix[vec_idx, vec_idx] = dot(orth_matrix[:, vec_idx], matrix[:, vec_idx])
    end
    return (orth_matrix, r_matrix)
end # function

"""
    Q, R = qr_reorth_rank(matrix)

Perform QR decomposition on the input matrix `matrix` using a modified
Gram-Schmidt orthogonalization with reorthogonalization and capability to handle
rank deficient matrices
"""
function qr_reorth_rank(matrix, reorth_threshold=10, rank_deficiency_factor=10, update_r=false)
    # perform a QR decomposition using classical Gram Schmidt with reorthogonalization
    # We use the rule of thumb suggested by Rutishauser to decide when to reorthogonalise
    # Note that this version does not work for rank deficient setups!
    num_vectors = size(matrix)[2]
    orth_matrix = copy(matrix)
    r_matrix = zeros(num_vectors, num_vectors)
    for vec_idx = 1:num_vectors
        norm_orth_column = 0   
        perform_reorthogonalization = true
        while (perform_reorthogonalization)
            norm_current_column = norm(orth_matrix[:, vec_idx])    
            for span_base_idx = 1:(vec_idx - 1) # orthogonalization
                projection_length = dot(orth_matrix[:, span_base_idx], orth_matrix[:, vec_idx])
                if (norm_orth_column == 0)
                    # do not update R when performing reorthogonalization
                    # norm_orth_column is exactly 0 on the first pass thgough
                    # this ensures that R gets updated properly
                    r_matrix[span_base_idx, vec_idx] = projection_length
                elseif (update_r)
                    r_matrix[span_base_idx, vec_idx] += projection_length
                end
                orth_matrix[:, vec_idx] -= projection_length * orth_matrix[:, span_base_idx]
            end
            norm_orth_column = norm(orth_matrix[:, vec_idx])
            # Handle rank deficiency and check if reorthogonalization is necessary
            if ((norm_orth_column < norm_current_column / reorth_threshold) &&
                (norm_orth_column > rank_deficiency_factor * eps() * norm_current_column))
                perform_reorthogonalization = true
            else
                perform_reorthogonalization = false
                if (norm_orth_column <= rank_deficiency_factor * eps() * norm_current_column)
                    orth_matrix[:, vec_idx] .= 0
                end
            end
        end
        if (norm(orth_matrix[:, vec_idx]) > eps())
            # linear dependend columns, we cannot normalise
            orth_matrix[:, vec_idx] = orth_matrix[:, vec_idx] / norm(orth_matrix[:, vec_idx])
        else
            orth_matrix[:, vec_idx] .= 0
        end
        r_matrix[vec_idx, vec_idx] = dot(orth_matrix[:, vec_idx], matrix[:, vec_idx])
    end
    return (orth_matrix, r_matrix)
end # function

end # module
