module QRGivens

using LinearAlgebra

export givens_qr

function givens_transform(a, b)
    # https://en.wikipedia.org/wiki/Givens_rotation
    # if (abs(b) > 10*eps())
    #     r = hypot(a, b)
    #     c = a / r
    #     s = - b / r
    # else
    #     c = 1
    #     s = 0
    #     r = a
    # end
    # return (c, s, r)
    if (abs(b) < eps())
        c = sign(a)
        if (c == 0)
            c = 1.0
        end
        s = 0
        r = abs(a)
    elseif (abs(a) < eps())
        c = 0
        s = sign(b)
        r = abs(b)
    elseif (abs(a) > abs(b))
        t = b / a
        u = sign(a) * hypot(1.0, t)
        c = 1 / u
        s = c * t
        r = a * u
    else
        t = a / b
        u = sign(b) * hypot(1.0, t)
        s = 1 / u
        c = s * t
        r = b * u
    end
    return (c, s, r)
end

function givens_rotation(row_idx, col_idx, dim, cosine, sine)
    G = Matrix{Float64}(I, dim, dim)
    G[row_idx, row_idx] = cosine
    G[col_idx, col_idx] = cosine
    G[row_idx, col_idx] = -sine
    G[col_idx, row_idx] = sine
    return G
end

function givens_qr(matrix)
    (nr, nc) = size(matrix)
    orth_matrix = Matrix{Float64}(I, nr, nc)
    r_matrix = copy(matrix)
    for col_idx = 1:nc
        for row_idx = (col_idx + 1):nr
            (cosine, sine, hypothenuse) = givens_transform(r_matrix[col_idx, col_idx], r_matrix[row_idx, col_idx])
            givens_rotation_matrix = givens_rotation(row_idx, col_idx, nc, cosine, sine)
            r_matrix = givens_rotation_matrix' * r_matrix
            orth_matrix = orth_matrix * givens_rotation_matrix
        end
    end
    return (orth_matrix, r_matrix)
end

end # module
